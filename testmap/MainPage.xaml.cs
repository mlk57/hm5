﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace testmap
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void Sat(object sender, EventArgs e)
        {
            map.MapType = MapType.Satellite;
        }

        public void Str(object sender, EventArgs e)
        {
            map.MapType = MapType.Street;
        }
        public void Hyb(object sender, EventArgs e)
        {
            map.MapType = MapType.Hybrid;
        }
    }
}
